alphabet = {
	"01" : "A",
	"1000" : "B",
	"1010" : "C",
	"100" : "D",
	"0" : "E",
	"0010" : "F",
	"110" : "G",
	"0000" : "H",
	"00" : "I",
	"0111" : "J",
	"101" : "K",
	"0100" : "L",
	"11" : "M",
	"10" : "N",
	"111" : "O",
	"0110" : "P",
	"1101" : "Q",
	"010" : "R",
	"000" : "S",
	"1" : "T",
	"001" : "U",
	"0001" : "V",
	"011" : "W",
	"1001" : "X",
	"1011" : "Y",
	"1100" : "Z"
}
class NotInAlphabetError(Exception):
	def __init__(self, value):
		self.value = value

def morse_to_char(morse):
	if morse in alphabet:
		return alphabet[morse]
	else:
		raise NotInAlphabetError(morse + "is no valid morse code.")

def morse_string(morse):
	#mstrings = morse.split(str=" ", num=morse.count(" "))
	mstrings = morse.split(" ")
	string = ""
	for str in mstrings:
		string += morse_to_char(str)
	return string
