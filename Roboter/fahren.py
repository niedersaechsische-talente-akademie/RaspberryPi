import RPi.GPIO as GPIO

from sensoren import *
from motor import *
#from blink import *
from farbe import *

GPIO.setwarnings(False)

faktor = 0.077
abstand = 0.17
distanz = 1
zeit = time.time()
pause = 0.1
farbe_getestet = 0

while True:
	farbe_getestet = 0
	i=int((ad(0)-ad(1))*faktor)
	while (i==0) and distanz > abstand:
		fahren(100,"l")
		fahren(100,"r")
		if (time.time() - zeit) >= pause:
			distanz = schall()
			zeit = time.time()
		i=int((ad(0)-ad(1))*faktor)
	while (i > 0) and distanz > abstand:
		fahren(100,"l")
		fahren(100-i,"r")
		if (time.time() - zeit) >= pause:
                        distanz = schall()
                        zeit = time.time()
		i=int((ad(0)-ad(1))*faktor)	
	while (i < 0) and distanz > abstand:
		fahren(100+i,"l")
       		fahren(100,"r")
		if (time.time() - zeit) >= pause:
                        distanz = schall()
                        zeit = time.time()
		i=int((ad(0)-ad(1))*faktor)
	while distanz <= abstand:
		stop("r")
		stop("l")
		if (time.time() - zeit) >= pause:
			distanz = schall()
                	zeit = time.time()
		if farbe_getestet == 0:
			farbe_getestet = 1
			farbe_erkennen()
	farbe_getestet == 0
