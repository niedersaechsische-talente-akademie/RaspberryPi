# Roboter Code

Here you find all the code which has to be uploaded to the robot.

### How to use this code?
If you run the code first time, you need to edit the IP for the TCP Server Connection. You will find these information in the **client.py**

```sh
TCP_IP = "your IP here"
TCP_PORT = your Port here
```

Simply run the **fahren.py** (best with sudo)
```sh
sudo python fahren.py
```
