import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)
GPIO.setup(27,GPIO.OUT)
GPIO.setup(22,GPIO.OUT)

def led():
	while 1:
		GPIO.output(27,GPIO.HIGH)
		GPIO.output(22,GPIO.LOW)
		time.sleep(0.1)
		GPIO.output(27,GPIO.LOW)
		GPIO.output(22,GPIO.HIGH)
		time.sleep(0.1)
