import RPi.GPIO as GPIO
import time
import smbus

GPIO.setmode(GPIO.BCM)
GPIO.setup(4, GPIO.OUT)
GPIO.setup(17, GPIO.IN)

#Software SPI
cs = 8
clk = 9
miso = 11
mosi = 7
GPIO.setup(cs, GPIO.OUT)
GPIO.setup(clk, GPIO.OUT)
GPIO.setup(miso, GPIO.IN)
GPIO.setup(mosi, GPIO.OUT)
GPIO.output(cs, GPIO.HIGH)

def schall():
	GPIO.output(4, GPIO.LOW)
	time.sleep(0.001)
	GPIO.output(4, GPIO.HIGH)
	time.sleep(0.00001)
	GPIO.output(4, GPIO.LOW)
	while GPIO.input(17) == GPIO.LOW:
		pass
	zeit_start = time.time()
	while GPIO.input(17) == GPIO.HIGH:
		if time.time() - zeit_start >= 0.006:
			break 
	zeit_ende=time.time()
	return ((zeit_ende - zeit_start)*346.3)/2	

def ad(kanal):	
	ausgabe_sensor = ""
	spi_frequenz = 1.35*10**6
	kanal_dual = ""	

	GPIO.output(cs, GPIO.HIGH)	
	GPIO.output(clk, GPIO.LOW)
	GPIO.output(mosi, GPIO.LOW)

	
	GPIO.output(cs, GPIO.LOW)
	time.sleep(10**(-7)) 					#100ns
	GPIO.output(mosi, GPIO.HIGH)
	
	GPIO.output(clk, GPIO.HIGH)
	time.sleep(1/2*spi_frequenz) 				#Haelfte der Periodendauer an (Clock pin (rechteckssignal))
	GPIO.output(clk, GPIO.LOW)
	time.sleep(1/2*spi_frequenz)
	GPIO.output(clk, GPIO.HIGH)
	time.sleep(1/2*spi_frequenz)	
	GPIO.output(clk, GPIO.LOW)
	

	if len(bin(kanal)[2:]) == 1:				#Uebersetzten des Kanals in dual (entfernen des 0b vor der Dualzahl)
		kanal_dual = "00"				#muss dreistellige Dualzahl sein, da drei Bits gesendet werden also 1 ist 001
		kanal_dual += bin(kanal)[2:]
		
	elif len(bin(kanal)[2:]) == 2:
                kanal_dual = "0"
                kanal_dual += bin(kanal)[2:]

	elif len(bin(kanal)[2:]) == 3:
                kanal_dual = bin(kanal)[2:]


	for i in range(3):					#Sende einzelne Bits um den Kanal anzusprechen ueber mosi
		if kanal_dual[i:].startswith("0"):		#Teste alle (for-Schleife) Dualziffern nacheinander auf 0 / 1			
			GPIO.output(mosi, GPIO.LOW)		#Sende 0 / 1 ueber mosi
			time.sleep(1/2*spi_frequenz)
		        GPIO.output(clk, GPIO.HIGH)
       			time.sleep(1/2*spi_frequenz)
       			GPIO.output(clk, GPIO.LOW)
 	
		elif  kanal_dual[i:].startswith("1"):
                	GPIO.output(mosi, GPIO.HIGH)
			time.sleep(1/2*spi_frequenz)
                	GPIO.output(clk, GPIO.HIGH)
                	time.sleep(1/2*spi_frequenz)
                	GPIO.output(clk, GPIO.LOW)

	GPIO.output(mosi, GPIO.LOW)
	
	for i in range(2):
		time.sleep(1/2*spi_frequenz)
       		GPIO.output(clk, GPIO.HIGH)
        	if i==1 & GPIO.input(miso) != GPIO.LOW:		#Testet vom Sensor Uebertragenes Startbit nach einem "Leertakt". Startbit muss 0 sein
			print "Fehler bei der SPI Kommunikation"
		time.sleep(1/2*spi_frequenz)
        	GPIO.output(clk, GPIO.LOW)

	for i in range(10):
		        time.sleep(1/2*spi_frequenz)
                        GPIO.output(clk, GPIO.HIGH)
			ausgabe_sensor += str(GPIO.input(miso))	#Lesen der 10 Rueckgabedatenbits des Sensors (Binaer) im Takt (clock)
			time.sleep(1/2*spi_frequenz)
                        GPIO.output(clk, GPIO.LOW)

	GPIO.output(cs, GPIO.HIGH)
	return int(ausgabe_sensor, 2)				#Umrechnung in Dezimalzahl

def color():
	bus = smbus.SMBus(1)
	bus.write_byte(0x29,0x80|0x12)
	ver = bus.read_byte(0x29)
	if ver == 0x44:
		bus.write_byte(0x29,0x80|0x00)
		bus.write_byte(0x29,0x01|0x02) 
		bus.write_byte(0x29,0x80|0x14)
	data = bus.read_i2c_block_data(0x29,0)
	clear = data[1] << 8 | data[0]
	red = data[3] << 8 | data[2]
	green = data[5] << 8 | data[4]
	blue = data[7] << 8 | data[6]
	colors = [clear, red, green, blue]
	return colors	
