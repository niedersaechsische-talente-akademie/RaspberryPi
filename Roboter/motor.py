import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setup(18, GPIO.OUT)
GPIO.setup(15, GPIO.OUT)
GPIO.setup(14, GPIO.OUT)
GPIO.setup(23, GPIO.OUT)
GPIO.setup(24, GPIO.OUT)
GPIO.setup(25, GPIO.OUT)

pwmr_A = GPIO.PWM(18, 100)
pwmr_B = GPIO.PWM(15, 100)
pwml_A = GPIO.PWM(24, 100)
pwml_B = GPIO.PWM(25, 100)	

def fahren(speed, motor):
	if speed > 0:
		vorwaerts(speed, motor)
	elif speed < 0:
		rueckwaerts((speed*(-1)), motor)
	else:
		stop(motor)

def vorwaerts(speed, motor):
	if motor == 'r':
        	GPIO.output(14, GPIO.HIGH)
       		GPIO.output(18, GPIO.LOW)
       		pwmr_B.start(speed)
	elif motor == 'l':
		GPIO.output(23, GPIO.HIGH)
		GPIO.output(24, GPIO.LOW)
		pwml_B.start(speed)

def rueckwaerts(speed, motor):
	if motor == 'r':
		GPIO.output(14, GPIO.HIGH)
		GPIO.output(15, GPIO.LOW)
		pwmr_A.start(speed)
        elif motor ==  'l':
		GPIO.output(23, GPIO.HIGH)
		GPIO.output(25, GPIO.LOW) 
       		pwml_A.start(speed)

def stop(motor):
        if motor == 'r':
                GPIO.output(14, GPIO.LOW)
                GPIO.output(15, GPIO.LOW)
                pwmr_A.stop()
        elif motor ==  'l':
                GPIO.output(23, GPIO.LOW)
                GPIO.output(25, GPIO.LOW)
                pwml_A.stop()
