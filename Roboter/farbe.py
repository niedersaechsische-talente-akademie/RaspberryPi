from sensoren import *
from morse import *
from client import *
import time

differenz = 1000

def farbe_erkennen():
	time.sleep(3)
	farbe = color()
	if farbe[1] - farbe[3] >= differenz:
		dual()
		
	elif farbe[3] - farbe[1] >= differenz:
		morse()
		
	else:
		print "Es wurde keine Farbe erkannt"

def dual():
	string = ""
	print "DUAL"
	farbe = color()
	while farbe[1] - farbe[3] >= differenz:
		farbe = color()
	time.sleep(2.5)
	for i in range(8):
		farbe = color()
		if farbe[1] - farbe[3] >= differenz:
			string += "1"
			print "1"
		else:
			string += "0"
			print "0"
		time.sleep(2)
	print int(string, 2)
	open_door()

def morse():
	string = ""
	print "MORSE"
	warte = True
	dit = 1.5
	dah = 4.5
	farbe = color()
	while farbe[3] - farbe[1] >= differenz:
                farbe = color()
        time.sleep(1.5)
	zeit = time.time()
        while warte:
		if farbe[3] - farbe[1] >= differenz:
			zeit_start = time.time()
			while farbe[3] - farbe[1] >= differenz:
				farbe=color()
			zeit_stop = time.time()
			if zeit_stop - zeit_start <= dit + 1: 
				string += "0"
				print "dit"
			elif zeit_stop - zeit_start <= dah + 1:
				string += "1"
				print "dah"
		if farbe[3] - farbe[1] < differenz:
			zeit_start = time.time()
			while (farbe[3]-farbe[1] <= differenz) & warte:
				farbe=color()
				if time.time() - zeit_start > dah + 1:
					warte = False
                                	print "Abort"
			zeit_stop = time.time()
			if zeit_stop - zeit_start <= dit + 1:
                	        pass
	                elif zeit_stop - zeit_start <= dah + 1:
        	                string += " "
                	        print "PAUSE"

	print string	
	print morse_string(string)	
	print "Morse fertig"
	open_door()
