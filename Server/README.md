# Server Code

Here you find all the code which has to be uploaded to the server / door.

### How to use this code?
If you run the code first time, you maybe want to edit the Port for the TCP Server where it is listening to connections. You will can change these information in the **server.py**

```sh
server.bind(("", <your port here>))
```
We recommend you to leave the quotation marks so the Server will always start with the IP he has.


Then simply run the **tor.py** (best with sudo)
```sh
sudo python tor.py
```
