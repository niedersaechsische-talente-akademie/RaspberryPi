import RPi.GPIO as GPIO
import time
from server import *


GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
enable = 2
motor_1 = 3 
motor_2 = 4
taster_1 = 14
taster_2 = 15

GPIO.setup(taster_1, GPIO.IN)
GPIO.setup(taster_2, GPIO.IN)
GPIO.setup(enable, GPIO.OUT)
GPIO.setup(motor_1, GPIO.OUT)
GPIO.setup(motor_2, GPIO.OUT)
GPIO.output(enable, GPIO.LOW)
GPIO.output(motor_1, GPIO.LOW)
GPIO.output(motor_2, GPIO.LOW)

def auf():
	while GPIO.input(taster_2) == GPIO.LOW:
		GPIO.output(enable, GPIO.HIGH)
		GPIO.output(motor_1, GPIO.HIGH)
		GPIO.output(motor_2, GPIO.LOW)

	GPIO.output(enable, GPIO.LOW)
	GPIO.output(motor_1, GPIO.LOW)
	GPIO.output(motor_2, GPIO.LOW)

def zu():
	while GPIO.input(taster_1) == GPIO.LOW:
        	GPIO.output(enable, GPIO.HIGH)
        	GPIO.output(motor_1, GPIO.LOW)
        	GPIO.output(motor_2, GPIO.HIGH)

	GPIO.output(enable, GPIO.LOW)
	GPIO.output(motor_1, GPIO.LOW)
	GPIO.output(motor_2, GPIO.LOW)


while 1:
	starte_server()	
	print "Tor oeffnen"
	auf()
	time.sleep(5)
	print "Tor schliessen"
	zu()
	time.sleep (0.1)
