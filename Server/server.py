import socket
import time

def starte_server():
	print "-----------------"
	erfolg = 0
	while erfolg == 0:
		try:
			server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			server.bind(("", 5449))
		        print "Socket erstellt und gebunden"
			server.listen(1)
		        server.accept()
		        print "Verbindung bestaetigt"
			erfolg = 1
		except:
			print "FEHLER SOCKET / BINDEN"
			server.close()
			time.sleep(3)
	server.close()
